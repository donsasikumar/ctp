package bbk.ctp.utils;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by don on 12/07/2015.
 */
public class CSVCreater {

    public static List<String> readSymbols(String textFile) throws FileNotFoundException {
        Scanner s = new Scanner(new File(textFile));
        ArrayList<String> list = new ArrayList<String>();
        while (s.hasNext()) {
            list.add(s.next());
        }
        s.close();
        return list;
    }

    public static void createCSVs(List<String> listOfSymbols, File toFile, String location){
        for(String name:listOfSymbols){
            saveToFile(new File(name+".csv"),"http://real-chart.finance.yahoo.com/table.csv?s="+name+"&d=5&e=19&f=2015&g=d&a=4&b=24&c=2014&ignore=.csv");
        }
        listOfSymbols.clear();
    }

    public static void saveToFile(File saveFile,String location){
        URL url;
        try {
            url = new URL(location);
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
            BufferedWriter out = new BufferedWriter(new FileWriter(saveFile));
            char[] cbuf=new char[255];
            while ((in.read(cbuf)) != -1) {
                out.write(cbuf);
            }
            in.close();
            out.close();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
