package bbk.ctp.utils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

/**
 * Created by don on 13/07/2015.
 */
public class Symbol {
    String id;
    Map<Date, HPrice> priceMap;

    public Symbol(String id, Map<Date, HPrice> priceMap) {
        this.id = id;
        this.priceMap = priceMap;
    }

    @Override
    public String toString() {
        return "Symbol{" +
                "id='" + id + '\'' +
                ", priceMap=" + priceMap +
                '}';
    }
}

class HPrice{
    BigDecimal Open;
    BigDecimal High;
    BigDecimal Low;
    BigDecimal Close;
    double Volume;
    BigDecimal AdjClose;

    public HPrice(BigDecimal open, BigDecimal high, BigDecimal low, BigDecimal close, double volume, BigDecimal adjClose) {
        Open = open;
        High = high;
        Low = low;
        Close = close;
        Volume = volume;
        AdjClose = adjClose;
    }

    @Override
    public String toString() {
        return "HPrice{" +
                "Open=" + Open +
                ", High=" + High +
                ", Low=" + Low +
                ", Close=" + Close +
                ", Volume=" + Volume +
                ", AdjClose=" + AdjClose +
                '}';
    }
}
