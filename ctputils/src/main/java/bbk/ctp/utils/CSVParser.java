package bbk.ctp.utils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by don on 13/07/2015.
 */
public class CSVParser {

    public static List<Symbol> parse(String fileName) throws ParseException,NumberFormatException {
        BufferedReader br = null;
        String line = "";
        String splitChar = ",";
        List<Symbol> listOfSymbols = new ArrayList<Symbol>();
        Map<Date, HPrice> priceMap = new HashMap<Date, HPrice>();
        try {
            br = new BufferedReader(new FileReader(fileName));
            br.readLine();
            while ((line = br.readLine()) != null) {
                // use comma as separator
                String[] prices = line.split(splitChar);
                if (prices[0].length()==10 && prices.length == 7 && prices[3].length()< 10) {
                    //System.out.println("Historical Price [Date= " + prices[0] + " , Open=" + prices[1] + " , High= " + prices[2] + " , Low=" + prices[3] + " , Close= " + prices[4] + " , Volume=" + prices[5] + " , Adjacent Close= " + prices[6] + "]");
                    HPrice hPrice = new HPrice(new BigDecimal(prices[1]), new BigDecimal(prices[2]), new BigDecimal(prices[3]), new BigDecimal(prices[4]), Double.parseDouble(prices[5]), new BigDecimal(prices[6]));
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Date date = sdf.parse(prices[0]);
                    priceMap.put(date,hPrice);
                }

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        System.out.println("Done");
        listOfSymbols.add(new Symbol(fileName,priceMap));
        return listOfSymbols;
    }
}



