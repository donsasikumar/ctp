import bbk.ctp.utils.CSVCreater;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by don on 12/07/2015.
 */
public class CSVCreaterTest
{
    @Test
    public void test_readSymbols() throws Exception {
        Assert.assertNotNull(CSVCreater.readSymbols("src/main/resources/symbols.txt"));
    }

    @Test
    public void test_saveToFile() throws Exception {
        String symbol = CSVCreater.readSymbols("src/main/resources/symbols.txt").get(0);
        String location = "http://real-chart.finance.yahoo.com/table.csv?s="+symbol+"&d=5&e=19&f=2015&g=d&a=4&b=24&c=2014&ignore=.csv";
        File file = new File("src/main/resources/csvhub/"+symbol+".csv");
        Assert.assertTrue(file.length()==0);
        CSVCreater.saveToFile(file, location);
        Assert.assertTrue(file.length() != 0);
        //file.delete();
    }
}
