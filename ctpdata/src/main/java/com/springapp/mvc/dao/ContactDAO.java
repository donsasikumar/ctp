package com.springapp.mvc.dao;



import com.springapp.mvc.dao.model.Contact;

import java.util.List;

/**
 * Created by don on 27/06/2015.
 */
public interface ContactDAO {
    public void saveOrUpdate(Contact contact);
    public void delete(int contactId);
    public Contact get(int contactId);
    public List<Contact> list();
}
