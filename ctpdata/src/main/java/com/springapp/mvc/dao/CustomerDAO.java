package com.springapp.mvc.dao;


import com.springapp.mvc.dao.model.Customer;

/**
 * Created by don on 27/06/2015.
 */
public interface CustomerDAO {
    public void insert(Customer customer);
    public Customer findByCustomer(int custId);
}
